#!/usr/bin/env python3
# Convert Mercurial projects on bitbucket.org to Git
# Uses the bridge: https://github.com/felipec/git-remote-hg
#
# FORMATTING: black -t py35 -l 120

try:
    import argparse
    import datetime
    import os
    import subprocess
    import sys
    import re
    import traceback
    import base64
    import json
    import urllib.parse, urllib.request
    from pathlib import Path
    from contextlib import contextmanager
except ImportError as ex:
    print("FATAL: library import error [{}] - Python version 3.5+ is required [{}].".format(ex, sys.version))
    exit(1)

# ----------------------------------------
# Encapsulates the BitBicket API to handle authentication and provide higher-level access methods.
#
# For API @see  https://developer.atlassian.com/bitbucket/api/2/reference/resource/
# For partial-response field selection @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/partial-response
# For filtering and sorting parameters @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering
# For pagination parameters @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
# Authentications uses 'App Passwords' @see https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html
# also @see https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication
#
class BBRestApi:
    def __init__(self, apiBaseUrl, **authArgs):
        self.baseUrl = apiBaseUrl
        self.username = authArgs.get("username")
        encodedAuth = base64.b64encode("{username}:{secret}".format(**authArgs).encode()).decode()
        self.authHeader = ("Authorization", "Basic {}".format(encodedAuth))
        self.jsonContentType = ("Content-Type", "application/json")

    # ----------------------------------------
    # Fetch all repositories for username.
    # Iterates through each page of results to collect all respositories.
    def getAllRepositories(self, queryParams):
        lastPage = False
        nextUrl = None
        totalSize = 0
        totalRead = 0
        allRepositories = []

        while not lastPage:
            if not nextUrl:
                data = self.getRepositoriesPaged(queryParams)
            else:
                data = self.getURL(nextUrl)
            nextUrl = data.get("next", None)
            lastPage = nextUrl == None
            values = data.get("values")
            if not values:
                log("No values returned.")
                break
            allRepositories.extend(values)
            totalRead += len(values)
            totalSize = data.get("size", 0)
            page = data.get("page")
            log("Read {}/{} results beginning at page {}; lastPage={}".format(totalRead, totalSize, page, lastPage))

        return allRepositories

    # ----------------------------------------
    # Fetch repositories for username.
    # @see https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D
    def getRepositoriesPaged(self, queryParams):
        uri = "2.0/repositories/{}".format(self.username)
        return self.getURI(uri, queryParams)

    # ----------------------------------------
    # Query for a JSON response from the given URI
    def getURI(self, uri, queryParams):
        url = "{}/{}".format(self.baseUrl, uri)
        if queryParams:
            url = "{}?{}".format(url, urllib.parse.urlencode(queryParams))
        return self.getURL(url)

    # ----------------------------------------
    # Fetch a JSON response from the given URL
    # Intended for use with pagination when BB gives the complete 'next' URL
    # FIXME: decode+return 40x error responses
    def getURL(self, url):
        result = None
        response = None
        try:
            headers = dict([self.authHeader])
            request = urllib.request.Request(url, headers=headers)
            response = urllib.request.urlopen(request)
            url = response.geturl()  # may have changed in case of redirect
            code = response.getcode()
            log("getURL [{}] response code:{}".format(url, code))
            content = response.read()
            content = content.decode("utf-8")  # Pre Python 3.6 hack (Raspberry Pi in 2019 still uses older release)
            result = json.loads(content)

        except urllib.error.HTTPError as ex:
            result = {"error": {"code": ex.code, "msg": ex.reason, "url": ex.url}}
            log("getURL HTTPError:{} headers:\n{}".format(str(ex), ex.headers), ex)

        except urllib.error.URLError as ex:
            result = {"error": {"code": -1, "msg": str(ex), "url": ex.url}}
            log("getURL URLError:{} headers:\n{}".format(str(ex), ex.headers), ex)

        except IOError as ex:
            result = {"error": {"code": -1, "msg": str(ex)}}
            log("getURL IOError:{} headers:\n{}".format(str(ex), ex.headers), ex)

        finally:
            if response != None:
                response.close()
        return result

    # ----------------------------------------
    # Create a new repository
    def createRepository(self, repository):
        uri = "2.0/repositories/{}/{}".format(self.username, repository.get("slug"))
        return self.postURI(uri, repository)

    # ----------------------------------------
    # POST a JSON request to the given URI
    def postURI(self, uri, postData):
        url = "{}/{}".format(self.baseUrl, uri)
        response = None
        result = None
        try:
            encodedData = json.dumps(postData).encode("ascii")
            headers = dict([self.authHeader, self.jsonContentType])
            request = urllib.request.Request(url, data=encodedData, headers=headers, method="POST")
            response = urllib.request.urlopen(request)
            url = response.geturl()  # may have changed in case of redirect
            code = response.getcode()
            log("postURI [{}] response code:{}".format(url, code))
            content = response.read()
            content = content.decode("utf-8")  # Pre Python 3.6 hack (Raspberry Pi in 2019 still uses older release)
            result = json.loads(content)

        except urllib.error.HTTPError as ex:
            result = {"error": {"code": ex.code, "msg": ex.reason, "url": ex.url}}
            log("postURI HTTPError:[{}] {} headers:\n{}".format(ex.code, ex.reason, ex.headers), ex)

        except urllib.error.URLError as ex:
            result = {"error": {"code": -1, "msg": str(ex), "url": ex.url}}
            log("postURI URLError:{} url:{}, headers:\n{}".format(str(ex), ex.url, ex.headers), ex)

        except IOError as ex:
            result = {"error": {"code": -1, "msg": str(ex)}}
            log("postURI IOError:{} headers:\n{}".format(str(ex), ex.headers), ex)

        finally:
            if response != None:
                response.close()
        return result


# ----------------------------------------
# Help with the repository migration
class RepositoryMigrationHelper:
    def __init__(self, workdir, repository, **authArgs):
        self.workdir = workdir
        self.repo = repository
        self.preferredAuth = "https" if (authArgs.get("https") == True) else "ssh"
        self.username = authArgs.get("username")
        self.password = authArgs.get("secret")

    # ----------------------------------------
    # Clone the Mercurial project using Git
    # Set the new Git repo remote
    # Push to the new Git repo
    def migrateHg2Git(self, gitRepo):
        hgUrl = self.findCloneUrl(self.repo)
        gitUrl = self.findCloneUrl(gitRepo)
        if gitUrl.startswith("https://"):
            gitUrl = gitUrl.replace(
                "https://{}@bitbucket.org/".format(self.username),
                "https://{}:{}@bitbucket.org/".format(self.username, self.password),
            )
        repodir = self.repo["slug"] + ".git"
        gitClone = "git clone --mirror hg::{} {}".format(hgUrl, repodir).split(" ")
        gitPush = "git push --mirror {}".format(gitUrl).split(" ")
        log("Migrating from hg::{} to git::{} ...".format(hgUrl, gitUrl))
        try:
            print(gitClone)
            subprocess.run(gitClone, check=True)
            with cwd(repodir):
                try:
                    print(gitPush)
                    subprocess.run(gitPush, check=True)
                    status = {"code": 0, "msg": "migration successful"}
                except subprocess.CalledProcessError as ex2:
                    status = {"code": ex2.returncode, "msg": "push failed", "cmd": ex2.cmd}
        except subprocess.CalledProcessError as ex1:
            status = {"code": ex1.returncode, "msg": "clone failed", "cmd": ex1.cmd}

        return status

    # ----------------------------------------
    # Create an equivalent repository with -git suffix
    def buildGitEquivalent(self):
        gitRepo = {
            "scm": "git",
            "slug": re.sub(r"(?i)(.+)(-hg)?$", r"\1-git", self.repo["slug"]),
            "full_name": re.sub(r"(?i)(.+)(-hg)?$", r"\1-git", self.repo["full_name"]),
            "name": re.sub(r"(?i)(.+)([ -]hg)?$", r"\1 (git)", self.repo["name"]),
            "description": "{} (Git conversion)".format(self.repo["description"]),
            "is_private": self.repo["is_private"],
            "fork_policy": self.repo["fork_policy"],
            "language": self.repo["language"],
            "has_issues": self.repo["has_issues"],
            "has_wiki": self.repo.get("has_wiki"),
            "website": self.repo.get("website"),
            "links": {},
        }
        if "mainbranch" in self.repo:
            gitRepo["mainbranch"] = self.repo["mainbranch"]
        if "avatar" in self.repo["links"]:
            gitRepo["links"]["avatar"] = self.repo["links"]["avatar"]
        if "parent" in self.repo:
            gitRepo["parent"] = self.repo["parent"]
        else:
            gitRepo["parent"] = {
                "type": "repository",
                "name": self.repo["name"],
                "full_name": self.repo["full_name"],
                "uuid": self.repo["uuid"],
            }
        return gitRepo

    # ----------------------------------------
    def printRepository(self):
        cloneUrl = self.findCloneUrl(self.repo)
        pp = "private" if repo.get("is_private") else "public "
        print(
            "{} {} {} URL:{} Name:{}".format(
                pp, self.repo.get("scm"), self.repo.get("slug"), cloneUrl, self.repo.get("name")
            )
        )

    # ----------------------------------------
    # Find the clone URL of a repository, honouring 'preferredAuth' protocol
    def findCloneUrl(self, repo):
        url = None
        for item in repo.get("links", {}).get("clone", {}):
            url = item["href"]
            proto = item["name"]
            if proto == self.preferredAuth:
                break
        return url

    # ----------------------------------------
    # @return True if repo is Mercurial
    def isHgRepo(self):
        return self.repo.get("scm") == "hg"


# ----------------------------------------
# Contextual change working directory
# with cwd(newdir):
#     ...do work...
@contextmanager
def cwd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        print("Working directory:", os.getcwd())
        yield
    finally:
        os.chdir(prevdir)
        print("Working directory:", os.getcwd())


# ----------------------------------------
# Search the PATH for file 'name'
# Check that it has 'execute' permission.
# Returns the full pathname if found and correct.
# FIXME: does not work on Windows - must also iterate through PATHEXT extensions.
def searchOsPath(name):
    found = None
    path = os.environ["PATH"]
    for entry in path.split(os.pathsep):
        target = os.path.join(entry, name)
        if os.path.isfile(target):
            if os.access(target, os.X_OK):
                found = target
                break
    return found


# ----------------------------------------
# Ensure work dir exists, create it if necessary.
# Throw an error if it exists but is not a directory.
# @return Path(workdir)
def checkWorkDir(workdir):
    workdir = Path(workdir)
    if workdir.exists():
        if not workdir.is_dir():
            raise ValueError("Workdir {} exists but is not a directory".format(workdir))
    else:
        workdir.mkdir(parents=True)
        print("Created workdir: {}".format(workdir))
    return workdir


# ----------------------------------------
# Check the minimum Python version is running
def checkPython():
    if sys.version_info[0] != 3 or sys.version_info[1] < 5:
        raise ValueError("This program requires Python 3.5+ to run correctly [{}]".format(sys.version))


# ----------------------------------------
# log message with a timestamp
def log(text, exception=None):
    sys.stderr.write("[{}] {}\n".format(datetime.datetime.utcnow().isoformat(), text))
    if exception != None:
        traceback.print_exc(file=sys.stderr)
        sys.stderr.write("\n")


# ----------------------------------------
# Program options parsing and help
def parseArgs():
    parser = argparse.ArgumentParser(
        description="Migrate BitBucket Mercurial repositories to Git",
        epilog="""
Recommend using a BitBucket App password for the migration - not your login password.  
Please read https://bitbucket.org/edrandall/bitbucket-hg-to-git/wiki/Home for more information.
            """,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-u", "--user", required=True, help="BitBucket username")
    parser.add_argument("-p", "--password", required=True, help="BitBucket password")
    parser.add_argument("-w", "--workdir", default="work", help="Work directory for conversions (default:'work')")
    parser.add_argument("--https", action="store_true", help="use https:// instead of ssh:// for repository cloning")
    parser.add_argument("-a", "--all", action="store_true", help="Find and convert ALL Mercurial repositories")
    parser.add_argument("slug", nargs="*", help="List of repository 'slugs' to be converted")
    args = parser.parse_args()
    if args.all:
        if len(args.slug) > 0:
            print("\nERROR: --all conflicts with list of repository 'slugs'\n")
            parser.print_usage()
            exit(1)
    else:
        if len(args.slug) < 1:
            print("\nERROR: List of repository 'slugs' is required, or specify --all\n")
            parser.print_usage()
            exit(1)

    return args


# ----------------------------------------
# Main program start
# Approximate flow:
# Ensure git is configured and git-remote-hg is on PATH
# https://github.com/felipec/git-remote-hg
# Connect (authenticate) to BitBucket API
# Read list of projects for user
# For each project:
# 	If it's a Mercurial project:
# 		If a Git equivalent exists already
# 			skip
# 		Create Git equivalent on BitBucket
# 		Clone project using git clone --mirror using hg:: prefix
# 		git push --mirror to new git project remote

# Handle command-line args
args = parseArgs()

try:
    checkPython()
except Exception as ex:
    print("FATAL: {} - cannot continue.".format(ex))
    exit(2)

# Check that required tools have been installed
for tool in ["git", "git-remote-hg"]:
    search = searchOsPath(tool)
    if search != None:
        print("Found {} at {}".format(tool, search))
    else:
        print("FATAL: Missing {} - cannot continue.".format(tool))
        exit(3)

try:
    workdir = checkWorkDir(args.workdir)
except Exception as ex:
    print("FATAL: {} - cannot continue.".format(ex))
    exit(4)

# Read list of all repositories for user
bbApi = BBRestApi("https://api.bitbucket.org", username=args.user, secret=args.password)
allRepositories = bbApi.getAllRepositories({"fields": "-*.owner.*"})
repoLookup = {repo["slug"]: repo for repo in allRepositories}

# Build list of repositories to be converted
repositories = []
if args.all:
    repositories = allRepositories
else:
    for slug in args.slug:
        repo = repoLookup.get(slug)
        if repo:
            repositories.append(repo)
        else:
            print("WARNING: repository identified by '{}' not found.".format(slug))

# Convert requested repositories
converted = 0
for repo in repositories:
    print("\nExisting repository: {} ({})".format(repo["full_name"], repo["name"]))
    reponame = repo["full_name"]
    helper = RepositoryMigrationHelper(workdir, repo, https=args.https, username=args.user, secret=args.password)
    if not helper.isHgRepo():
        log("Non-Hg repository - skipping {}".format(reponame))
        continue

    gitRepo = helper.buildGitEquivalent()
    print("New Git repository: {} ({})".format(gitRepo["full_name"], gitRepo["name"]))
    if gitRepo["slug"] in repoLookup:
        log("Conversion already done - skipping {}".format(reponame))
        continue

    log("Creating new Git repository:{}".format(gitRepo.get("slug")))
    newRepo = bbApi.createRepository(gitRepo)
    if newRepo.get("error") != None:
        log(
            "WARNING: Create repository {} failed:{} - skipping {}".format(
                gitRepo.get("slug"), newRepo["error"]["msg"], reponame
            )
        )
        continue

    status = helper.migrateHg2Git(newRepo)
    if status["code"] == 0:
        converted += 1
        log("Repository {} conversion succeeded.\n".format(reponame))
    else:
        log("ERROR: Repository {} conversion failed: [{}] {}\n".format(reponame, status["code"], status["msg"]))


log("Converted {} repositories.".format(converted))
